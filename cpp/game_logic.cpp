#include "game_logic.h"
#include "protocol.h"

using namespace hwo_protocol;
using namespace jsoncons;

game_logic::game_logic()
  : action_map
    {
      { "join", &game_logic::on_join },
      { "gameStart", &game_logic::on_game_start },
      { "carPositions", &game_logic::on_car_positions },
      { "crash", &game_logic::on_crash },
      { "gameEnd", &game_logic::on_game_end },
      { "error", &game_logic::on_error },
      { "yourCar", &game_logic::on_your_car },
      { "gameInit", &game_logic::on_game_init },
    }
{
}

game_logic::msg_vector game_logic::react(const jsoncons::json& msg)
{
  const auto& msg_type = msg["msgType"].as<std::string>();
  const auto& data = msg["data"];
  auto action_it = action_map.find(msg_type);
  if (action_it != action_map.end())
  {
    return (action_it->second)(this, data);
  }
  else
  {
    std::cout << "Unknown message type: " << msg_type << std::endl;
    return { make_ping() };
  }
}

game_logic::msg_vector game_logic::on_join(const jsoncons::json& data)
{
  std::cout << "Joined" << std::endl;
  return { make_ping() };
}

game_logic::msg_vector game_logic::on_game_start(const jsoncons::json& data)
{
  std::cout << "Race started" << std::endl;
  return { make_ping() };
}

game_logic::msg_vector game_logic::on_car_positions(const jsoncons::json& data)
{
  double throttle = 0.2;
  for (size_t i = 0; i < data.size(); ++i)
  {
    try
    {
        std::string currentName = data[i]["id"]["name"].as<std::string>();
        if (name.compare(currentName) == 0) {
            jsoncons::json pieces = data[i]["piecePosition"];
            int pieceIndex = (pieces.has_member("pieceIndex")) ? pieces["pieceIndex"].as<int>() : 0;
            if (pieceIndex < track.size()) {
              Piece piece = track[pieceIndex];   
              if (piece.radius > 0.1) {
                throttle = 0.5;
              } else {
                throttle = 0.99;
              }
            } 
            break;
        }
    }
    catch (const std::exception& e)
    {
        std::cerr << e.what() << std::endl;
    }
  }
  std::cout << "throttle" << throttle << std::endl; 
  return { make_throttle(throttle) };  
}

game_logic::msg_vector game_logic::on_crash(const jsoncons::json& data)
{
  std::cout << "Someone crashed" << std::endl;
  return { make_ping() };
}

game_logic::msg_vector game_logic::on_game_end(const jsoncons::json& data)
{
  std::cout << "Race ended" << std::endl;
  return { make_ping() };
}

game_logic::msg_vector game_logic::on_error(const jsoncons::json& data)
{
  std::cout << "Error: " << data.to_string() << std::endl;
  return { make_ping() };
}

game_logic::msg_vector game_logic::on_your_car(const jsoncons::json& data)
{
 /*{"msgType": "yourCar", "data": {
  "name": "Schumacher",
  "color": "red"
}}*/ 
  name =  data["name"].as<std::string>();
  color =  data["color"].as<std::string>();
  return { make_ping() };
}

game_logic::msg_vector game_logic::on_game_init(const jsoncons::json& data)
{
  
  jsoncons::json pieces = data["race"]["track"]["pieces"];
 
  for (size_t i = 0; i < pieces.size(); ++i)
  {
    try
    {
        json& pieceJson = pieces[i];
        Piece piece;

        piece.length = (pieceJson.has_member("length")) ? pieceJson["length"].as<double>() : 0;
        piece.radius = (pieceJson.has_member("radius"))  ? pieceJson["radius"].as<double>() : 0;
        piece.angle = (pieceJson.has_member("angle")) ? pieceJson["angle"].as<double>() : 0;
        piece.isSwitch = (pieceJson.has_member("switch")) ? pieceJson["switch"].as<bool>() : false;
        track.push_back(piece);
        std::cout <<  piece.length << ", " <<  piece.radius << ", " <<  piece.angle << std::endl;
    }
    catch (const std::exception& e)
    {
        std::cerr << e.what() << std::endl;
    }
  }
  return { make_ping() };
}